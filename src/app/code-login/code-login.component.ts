import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'code-login',
  templateUrl: './code-login.component.html',
  styleUrls: ['./code-login.component.scss']
})
export class CodeLoginComponent implements OnInit {

  codeForm!: FormGroup;
  audioPlayed: boolean = false;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.codeForm = this.formBuilder.group({
      code: [null, Validators.required]
    });
  }

  onCodeSubmit(): void {

  }

  playAudio(): void {
    if (this.audioPlayed)
      return;

    let audio = new Audio();
    audio.src = "../../assets/audio/intro.mp3";
    audio.load();
    audio.play();
    this.audioPlayed = true;
  }

  get code() {
    return this.codeForm.get('code')
  }

}
