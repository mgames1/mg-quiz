import { QuestionComponent } from './question/question.component';
import { CodeLoginComponent } from './code-login/code-login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'question/:id', component: QuestionComponent },
    { path: '**', component: CodeLoginComponent },
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }